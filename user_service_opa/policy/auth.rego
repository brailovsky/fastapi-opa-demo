package user_api

default allow = false

allow {
    input.method == "GET"
    input.path == ["api", "users"]
    is_user(input.payload)
}

# Allow creating users only for admin
allow {
    input.method == "POST"
    input.path == ["api", "users"]
    is_admin(input.payload)
}

is_user(p) {
    p.role == "user"
}

is_user(p) {
    p.role == "admin"
}

is_admin(p) {
    p.role == "admin"
}