FROM tiangolo/uvicorn-gunicorn-fastapi:python3.10

COPY . /app

RUN pip install --no-cache-dir /app && rm -rf /app

CMD ["users-start"]
