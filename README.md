# DEMO OPA with FastAPI User Service

This demonstration shows how to integrate Open Policy Agent (OPA) with a FastAPI user service, employing tools like curl, jq, kubectl, k9s, minikube, and Docker for setup and deployment.

## Prerequisites

- Install **kubectl**, **k9s**, **minikube**, and **Docker** on your machine.

## Setup Instructions

1. **Start Minikube**

    Execute the following command to start Minikube:

    ```bash
    minikube start
    ```

2. **Build Docker Container**

    Build the Docker container for the user service:

    ```bash
    docker build -t ostfor/user-service:latest .
    ```

    > **Note:** If using a private registry, replace `ostfor/user-service:latest` with your registry path in both the command above and the `deploy/user-service-deployment.yaml` file.

3. **Deploy OPA Policies and Secrets**

    Create a ConfigMap for OPA policy and a secret for the app:

    ```bash
    kubectl create configmap opa-policy --from-file=./user_service_opa/policy/auth.rego
    kubectl create secret generic app-secrets --from-literal=SECRET_KEY='<your_secret-key>'
    ```

4. **Apply Kubernetes Deployments**

    Deploy the OPA and user service components:

    ```bash
    kubectl apply -f deploy/opa-deployment.yaml
    kubectl apply -f deploy/opa-service.yaml
    kubectl apply -f deploy/user-service-deployment.yaml
    kubectl apply -f deploy/user-service.yaml
    ```

5. **Expose Ports for Testing**

    Forward the user service port for local testing:

    ```bash
    kubectl port-forward service/user-service 8000:8000
    ```

## Testing Process

1. **Get Token for Admin User**

    Obtain an access token for the admin user:

    ```bash
    ACCESS_TOKEN=$(curl -X POST "http://127.0.0.1:8000/oauth/token" -H "Content-Type: application/x-www-form-urlencoded" -d "username=admin&password=admin" | jq -r .access_token)
    ```

2. **Retrieve List of Users with Admin Token**

    Use the admin token to retrieve a list of users:

    ```bash
    curl -X 'GET' 'http://127.0.0.1:8000/api/users' -H "Content-Type: application/json" -H "Authorization: Bearer ${ACCESS_TOKEN}"
    ```

    **Output:**

    ```json
    {"name":"admin","email":"admin@example.com","role":"admin"}
    ```

3. **Add New User**

    Add a new user with the admin token:

    ```bash
    curl -X POST "http://127.0.0.1:8000/api/users" -H "Content-Type: application/json" -H "Authorization: Bearer ${ACCESS_TOKEN}" -d '{"name": "newuser", "email": "newuser@example.com", "password": "password123", "role": "user"}'
    ```

    **Output:**

    ```json
    {"name":"newuser","email":"newuser@example.com"}
    ```

4. **Get Token for New User**

    Obtain an access token for the new user:

    ```bash
    USER_ACCESS_TOKEN=$(curl -X POST "http://127.0.0.1:8000/oauth/token" -H "Content-Type: application/x-www-form-urlencoded" -d "username=newuser&password=password123" | jq -r .access_token)
    ```

5. **Attempt to Create New User with New User Token**

    Attempting to create another user with the new user token should fail:

    ```bash
    curl -X POST "http://127.0.0.1:8000/api/users" -H "Content-Type: application/json" -H "Authorization: Bearer ${USER_ACCESS_TOKEN}" -d '{"name": "newuser2", "email": "newuser2@example.com", "password": "password1234", "role": "user"}'
    ```

    **Output:**

    ```json
    {"detail":"Not authorized by OPA"}
    ```

6. **Retrieve List of Users with User Token**

    Retrieve a list of users using the new user's token:

    ```bash
    curl -X 'GET' 'http://127.0.0.1:8000/api/users' -H "Content-Type: application/json" -H "Authorization: Bearer ${USER_ACCESS_TOKEN}"
    ```

    **Output:**

    ```json
    [{"name":"admin","email":"admin@example.com","role":"admin"},{"name":"newuser","email":"newuser@example.com","role":"user"}]
    ```

## TODO Plan

- [ ] Add an endpoint to update user password. Users should be obligated to change their passwords after creation and periodically thereafter.
- [ ] Add an endpoint to update user info (like user email).
- [ ] Avoid hardcoding admin user on initialization.
- [ ] Add tests for the new features and existing functionality.
- [ ] Implement a health check service to monitor the status of the application.
- [ ] Integrate Terraform and CI for auto-deployment to a favorite cloud provider.
