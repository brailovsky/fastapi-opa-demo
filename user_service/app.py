
from fastapi import FastAPI
from .core.database import Base, engine
from .api import oauth, users
from .services.user import initialize

app = FastAPI()

app.include_router(oauth.router)
app.include_router(users.router)

async def create_tables():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)



@app.on_event("startup")
async def startup():
    await create_tables()
    await initialize()

