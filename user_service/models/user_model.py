from sqlalchemy import Column, Integer, String

from ..core.database import Base

class UserDB(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)
    email = Column(String, unique=True)
    role = Column(String, unique=False)
    hashed_password = Column(String)
