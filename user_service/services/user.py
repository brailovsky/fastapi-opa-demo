from typing import List
from sqlalchemy import select
from sqlalchemy.orm.exc import NoResultFound
from passlib.context import CryptContext
from ..core.database import get_db
from ..core.utils import UserRole
from ..models.user_model import UserDB
from ..schemas.user_schema import User, UserBase, UserForm, UserInDB

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def hash_password(password: str) -> str:
    """Hash a password for storing."""
    return pwd_context.hash(password)

def verify_password(plain_password: str, hashed_password: str) -> bool:
    """Verify a stored password against one provided by user."""
    return pwd_context.verify(plain_password, hashed_password)


async def get_user_by_username(username: str) -> UserInDB:
    async with get_db() as session:
        async with session.begin():
            query = select(UserDB).filter(UserDB.name == username)
            result = await session.execute(query)
            try:
                return UserInDB.model_validate(result.scalars().one())
            except NoResultFound:
                return None


async def create_user(user: UserForm):
    async with get_db() as session:
        hashed_password = hash_password(user.password)
        new_user = UserDB(
            name=user.name,
            hashed_password=hashed_password,
            email=user.email,
            role = user.role,
        )
        session.add(new_user)
        await session.commit()
        await session.refresh(new_user)
    return UserBase.model_validate(new_user)


async def is_admin_role(name: str) -> bool:
    async with get_db() as session:
        query = select(UserDB).filter(UserDB.name == name)
        result = await session.execute(query)
        user = result.scalars().first()
        return user.role == UserRole.ADMIN.value if user else False

async def is_user_role(name: str) -> bool:
    async with get_db() as session:
        query = select(UserDB).filter(UserDB.name == name)
        result = await session.execute(query)
        user = result.scalars().first()
        return user.role == UserRole.USER.value if user else False


async def get_all_users() -> List[User]:
    """ Get all users list from the database. """
    async with get_db() as session:
        query = select(UserDB)
        result = await session.execute(query)
        user_db_list = result.scalars().all()
        return [User.model_validate(user_db) for user_db in user_db_list]

async def initialize(
        default_admin_username="admin",
        admin_email = "admin@example.com",
        admin_password = "admin"
    ):
    """ Initialize the database with the default admin user. """
    existing_admin = await get_user_by_username(default_admin_username)

    if existing_admin is None:
        user = UserForm(
            name=default_admin_username,
            password=admin_password,
            email=admin_email,
            role=UserRole.ADMIN.value,
        )
        await create_user(user)