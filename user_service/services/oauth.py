from fastapi import HTTPException, Request, Security
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt

from ..core.settings import settings
from ..services.opa_policy_auth import query_opa
from ..services.roles import get_user_role
from ..schemas.token_schema import TokenData
from .user import get_user_by_username, verify_password

class AuthFailedError(Exception):
    ...

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
ALGO = "HS256"

async def authenticate_user(username: str, password: str):
    user = await get_user_by_username(username)
    if user and verify_password(password, user.hashed_password):
        return user
    raise AuthFailedError()


def create_access_token(data: TokenData):
    to_encode = data.model_copy()
    jwt_encoded = jwt.encode(to_encode.model_dump(), settings.SECRET_KEY, algorithm=ALGO)
    return jwt_encoded


async def get_current_user(request: Request, token: str = Security(oauth2_scheme)):
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=ALGO)
        token_data = TokenData(**payload)
    except JWTError:
        raise HTTPException(status_code=403, detail="Could not validate credentials")
    user_name = token_data.user
    user_role = await get_user_role(user_name)

    opa_response = await query_opa(request, user_role)
    if not opa_response.get("result", False):
        raise HTTPException(status_code=403, detail="Not authorized by OPA")
    return {"user": user_name, "role": user_role}
