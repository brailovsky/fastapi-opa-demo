import logging
from fastapi import Request
import httpx
from ..core.settings import settings
from ..schemas.opa_policy_schema import InputData, RolePayload

LOGGER = logging.getLogger(__file__)

async def query_opa(request: Request, role: str):
    path = request.url.path.lstrip('/').split('/')
    path = list(filter(None, path))
    action = request.method
    input_data = InputData(
        method=action,
        path=path,
        payload=RolePayload(role=role)
    )
    pload = {"input": input_data.model_dump()}
    LOGGER.warning(f"Authorisation attempt: {pload}")

    async with httpx.AsyncClient() as client:
        response = await client.post(
            settings.OPA_URL,
            json=pload
        )
    response_json = response.json()

    LOGGER.warning(f"Responce for {pload} is {response_json}")

    return response_json