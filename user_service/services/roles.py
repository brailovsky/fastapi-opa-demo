
from ..core.utils import UserRole
from ..schemas.user_schema import User
from .user import is_admin_role, is_user_role


async def get_user_role(user_name: User):
    if await is_admin_role(user_name):
        return UserRole.ADMIN.value
    if await is_user_role(user_name):
        return UserRole.USER.value
    else:
        return UserRole.UNAUTHORIZED.value