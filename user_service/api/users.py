
import logging
from fastapi import APIRouter, Depends, HTTPException
from ..schemas.user_schema import UserBase, UserForm
from ..services.oauth import get_current_user
from ..services.user import create_user, get_all_users, get_user_by_username
LOGGER = logging.getLogger(__file__)
router = APIRouter(
    prefix='/api',
    tags=['api']
)

@router.post("/users", response_model=UserBase)
async def api_create_user(
        user: UserForm,
        current_user: dict = Depends(get_current_user)
    ):
    db_user = await get_user_by_username(user.name)
    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    db_user = await create_user(user)
    LOGGER.warning(
        f"User {current_user.get('user')} successfully created {db_user.name}"
    )
    return db_user


@router.get("/users")
async def read_users(current_user: dict = Depends(get_current_user)):
    all_user = await get_all_users()
    LOGGER.warning(
        f"User {current_user.get('user')} successfully got list of users"
    )
    return all_user


