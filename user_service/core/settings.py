from typing import List
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    OPA_URL: str = "http://localhost:8181/v1/data/user_api/allow"
    SECRET_KEY: str = "secret_key"
    DATABASE_URL: str
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 30  # The expiration time for the token

    class Config:
        env_file = ".env"

settings = Settings()