
from enum import Enum


class UserRole(Enum):
    UNAUTHORIZED = "unauthorized"
    USER = "user"
    ADMIN = "admin"