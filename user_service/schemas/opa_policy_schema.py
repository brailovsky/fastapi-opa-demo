from typing import List
from pydantic import BaseModel

class RolePayload(BaseModel):
    role: str

class InputData(BaseModel):
    method: str
    path: List[str]
    payload: RolePayload

