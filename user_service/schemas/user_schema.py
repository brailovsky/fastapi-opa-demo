
from pydantic import BaseModel, Field, validator

from user_service.core.utils import UserRole

class UserBase(BaseModel):
    name: str
    email: str

    class Config:
        from_attributes = True

class User(UserBase):
    role: str = Field(
        default=UserRole.USER.value,
         description=f"User role can be one of {', '.join([role.value for role in UserRole])}" # noqa
    )

    @validator("role")
    def validate_role(cls, v):
        if v not in [role.value for role in UserRole]:
            raise ValueError("Invalid role. Role must be one of user, admin, moderator")
        return v

    class Config:
        from_attributes = True


class UserForm(User):
    password: str

class UserInDB(User):
    hashed_password: str

